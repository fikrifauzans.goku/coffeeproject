<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title', 'Admin Zahwa Cof')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset("adminLTE/plugins/fontawesome-free/css/all.min.css")}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset("adminLTE/dist/css/adminlte.min.css")}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{'adminLTE'}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{'adminLTE'}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{'adminLTE'}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  @stack('css')
</head>
<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  @include('layouts.navbar')

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{'adminLTE'}}/index3.html" class="brand-link">
      <img src="{{'adminLTE'}}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    @include('layouts.sidebar')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @yield('content')
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Title</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          Start creating your amazing application!
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.1.0
    </div>
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{'adminLTE'}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{'adminLTE'}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- adminLTELTE App -->
<script src="{{'adminLTE'}}/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{'adminLTE'}}/dist/js/demo.js"></script>
<!-- DataTables  & Plugins -->
<script src="{{'adminLTE'}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{'adminLTE'}}/plugins/jszip/jszip.min.js"></script>
<script src="{{'adminLTE'}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{'adminLTE'}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{'adminLTE'}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@stack('js')
</body>
</html>