@extends('layouts.index')

@section('content')
    
<form action="{{ route('admin.product.store') }}" method="Post">@csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Product Name</label>
      <input name='name' type="text" class="form-control"  placeholder="create name">
    </div>
    <div class="form-group">
      <label for="" class="control-label">Kategori</label>
      <select name="category_id" id="" class="form-control" required>
          <option value="" disabled selected>-- Pilih --</option>
          @forelse ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
          @empty
              <option value="" disabled>Tidak ada data</option>
          @endforelse
      </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Product Descripton</label>
        <input name='description'  type="text" class="form-control"  placeholder="create name">
      </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Status</label>
        <input name='stock' type="text" class="form-control"  placeholder="create name">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Price</label>
        <input name='price' type="number" class="form-control"  placeholder="create name">
    </div>
    <button type='submit' class="btn btn-primary">Submit</button>
  </form>



@endsection