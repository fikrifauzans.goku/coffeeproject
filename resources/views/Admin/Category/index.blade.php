@extends('layouts.index')

@section('name')
    @extends('layouts.index')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with default features</h3>

        <a href="{{ route('admin.Category.create') }}" class="btn btn-primary float-right">Create</a>

    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Description</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($category as $item_category)
                <tr>
                    
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item_category->name}}</td>
                    <td>{{$item_category->description}}</td>

                    
                    <td><a href='{{ route('admin.Category.edit', $item_category->id) }}' class="btn btn-primary">Edit</a>
                        
                        <form action="{{ route('admin.Category.delete', $item_category) }}" method="post">@csrf @method('delete')
                        <button type='submit' class="btn btn-primary">Delete</button>
                    </form>

                    <a href='{{ route('admin.Category.show', $item_category->id) }}' class="btn btn-primary">Detail</a></td>

                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('js')
    <script>
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    </script>
@endpush
@endsection