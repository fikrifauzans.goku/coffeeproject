@extends('layouts.index')

@section('content')
    
<form action="{{ route('admin.Category.update' , $item_category->id) }}" method="Post">@csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Category Name</label>
      <input name='name' type="text" class="form-control" value="{{ $item_category->name }}" placeholder="create name">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1"></label>
        <textarea class="form-control" name="description"  cols="30" rows="10" value=''>{{ $item_category->description }}</textarea>
      </div>
    <button type='submit' class="btn btn-primary">Submit</button>
  </form>



@endsection