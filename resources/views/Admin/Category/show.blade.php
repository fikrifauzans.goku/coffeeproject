@extends('layouts.index')

@section('content')

    <div class="card">
        <div class="card-header">{{ $category->name }}</div>
        <div class="card-body">
            <div class="row">
                @foreach ($category->products as $category_item)
                <div class="col-md-4">
                    <p><strong>Name: {{ $category_item->name }}</strong></p>
                    <p>Price: {{ $category_item->price }}</p>
                    <p>Stock: {{ $category_item->stock }}</p>
                    <p>Description: {{ $category_item->description }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection